terraform {
  required_version = "~> v1.4.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.4.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.0"
    }
  }
}

data "cloudflare_zone" "zone" {
  name = var.cf_zone_name
}

# The random_id resource is used to generate a 35 character secret for the tunnel
resource "random_id" "tunnel_secret" {
  byte_length = 35
}

resource "cloudflare_tunnel" "tunnel" {
  account_id = var.cf_account_id
  name       = var.cf_tunnel_name
  secret     = random_id.tunnel_secret.b64_std
}

resource "cloudflare_record" "service" {
  for_each = { for rule in var.ingress_rules : rule.hostname => rule }

  zone_id = data.cloudflare_zone.zone.zone_id
  name    = each.key
  value   = cloudflare_tunnel.tunnel.cname
  type    = "CNAME"
  ttl     = 1
  proxied = true
}

resource "cloudflare_tunnel_config" "config" {
  account_id = var.cf_account_id
  tunnel_id  = cloudflare_tunnel.tunnel.id

  config {
    warp_routing { # TODO Causes perpetual diff when used with no tunnel routes
      enabled = var.warp_routing
    }

    dynamic "ingress_rule" {
      for_each = var.ingress_rules
      content {
        hostname = "${ingress_rule.value.hostname}.${var.cf_zone_name}"
        path     = ingress_rule.value.path
        service  = ingress_rule.value.service
      }
    }
    ingress_rule {
      service = "http_status:404"
    }
  }
}

resource "cloudflare_tunnel_route" "routes" {
  for_each = var.tunnel_routes

  account_id = var.cf_account_id
  tunnel_id  = cloudflare_tunnel.tunnel.id
  network    = each.key
  comment    = each.value
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE THE INSTALL SCRIPT TO RUN ON THE INSTANCE
# This script will install cloudflared and configure using the Cloudflare API
# ---------------------------------------------------------------------------------------------------------------------

locals {
  user_data = templatefile(
    "${path.module}/scripts/script.sh",
    {
      tunnel_token = cloudflare_tunnel.tunnel.tunnel_token,
    },
  )
}

# ---------------------------------------------------------------------------------------------------------------------
# PROVISION AN EXISTING HOST
# ---------------------------------------------------------------------------------------------------------------------
resource "terraform_data" "provision" {
  triggers_replace = [
    cloudflare_tunnel.tunnel.id,
  ]

  for_each = toset(var.connector_hosts)

  # Establishes connection to be used by all
  # generic remote provisioners (i.e. file/remote-exec)
  connection {
    host  = each.key
    type  = "ssh"
    agent = true
    #    user  = var.ssh_user # TODO - Destroy-time provisioners and their connection configurations may only reference attributes of the related resource
  }

  provisioner "file" {
    content     = local.user_data
    destination = "./script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x ./script.sh",
      "sudo ./script.sh",
      "rm -rf ./script.sh"
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "if systemctl is-active --quiet cloudflared; then sudo cloudflared service uninstall; fi",
    ]
  }

  depends_on = [cloudflare_tunnel_config.config, ]
}
