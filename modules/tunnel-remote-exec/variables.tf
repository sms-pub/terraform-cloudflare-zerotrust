# Cloudflare Variables
variable "cf_account_id" {
  description = "The Cloudflare account ID that you wish to manage the Argo Tunnel on"
  type        = string
}

variable "cf_tunnel_name" {
  description = "A user-friendly name chosen when the tunnel is created"
  type        = string
  default     = "example-tunnel"
}

variable "cf_zone_name" {
  description = "The name of the zone"
  type        = string
}

variable "warp_routing" {
  description = "If you're exposing a private network, you need to add the warp-routing key and set it to true."
  type        = bool
  default     = false
}

variable "ingress_rules" {
  description = "Each incoming request received by Cloudflared causes Cloudflared to send a request to a local service. See - https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/install-and-setup/tunnel-guide/local/local-management/ingress/"
  type = list(object({
    hostname = string
    path     = string
    service  = string
  }))
  # Example
  # default = [
  #   {
  #     hostname = "myweb"
  #     path     = ""
  #     service  = "http://web-service:80"
  #   },
  #   {
  #     hostname = "hello"
  #     path     = ""
  #     service  = "hello_world"
  #   }
  # ]
}

variable "tunnel_routes" {
  description = "The IPv4 or IPv6 network for the tunnel route, in CIDR notation."
  type        = map(string)
  default = {
    # Example
    # "10.0.0.0/8" = "This is a description"
  }
}

# Connector Variables
variable "connector_hosts" {
  description = "Hosts to configure for connection to the Cloudflare tunnel"
  type        = list(string)
  default     = []
}

#variable "ssh_user" {
#  description = "Host ssh user name"
#  type        = string
#  sensitive   = true
#}
