#!/bin/bash
#
# Retrieve the cloudflared Linux package if not already installed
if which cloudflared
then
  echo "Found cloudflared"
else
  echo "Installing cloudflared"
  wget https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb
  sudo dpkg -i cloudflared-linux-amd64.deb
  rm -rf cloudflared-linux-amd64.deb
fi

# Increase receive buffer size. See - https://github.com/quic-go/quic-go/wiki/UDP-Receive-Buffer-Size
echo "net.core.rmem_max = 26214400" | sudo tee -a /etc/sysctl.conf
echo "net.core.rmem_default = 26214400" | sudo tee -a /etc/sysctl.conf

# Remove any existing cloudflared service
if systemctl is-active --quiet cloudflared
then
  sudo cloudflared service uninstall
fi

# Run a connector
sudo cloudflared service install ${tunnel_token}
