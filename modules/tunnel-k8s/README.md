# [tunnel-k8s](https://developers.cloudflare.com/cloudflare-one/tutorials/many-cfd-one-tunnel/)
This module uses a Cloudflare Tunnel (formerly Argo Tunnel) to route internet traffic into your Kubernetes cluster.

## Table of Contents
- [Usage](#usage)
- [Requirements](#requirements)
- [Providers](#providers)
- [Modules](#modules)
- [Resources](#resources)
- [Inputs](#inputs)
- [Outputs](#outputs)
- [Contributing](#contributing)

## Usage
```bash
terraform init
terraform plan
terraform apply
```

### Pre-requisites
<!-- Describe external dependencies or pre-requisites -->
- K8s Cluster
- Cloudflare Zero Trust

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> v1.4.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.4.0 |
| <a name="requirement_kubectl"></a> [kubectl](#requirement\_kubectl) | ~> 1.14.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.18.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | 4.4.0 |
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | 1.14.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.18.1 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.5.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.service](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_tunnel.tunnel](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/tunnel) | resource |
| [kubectl_manifest.cf_tunnel_configmap](https://registry.terraform.io/providers/gavinbunney/kubectl/latest/docs/resources/manifest) | resource |
| [kubectl_manifest.cf_tunnel_deployment](https://registry.terraform.io/providers/gavinbunney/kubectl/latest/docs/resources/manifest) | resource |
| [kubernetes_secret.cf_tunnel_creds](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [random_id.tunnel_secret](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [cloudflare_zone.zone](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cf_account_id"></a> [cf\_account\_id](#input\_cf\_account\_id) | The Cloudflare account ID that you wish to manage the Argo Tunnel on | `string` | n/a | yes |
| <a name="input_cf_tunnel_name"></a> [cf\_tunnel\_name](#input\_cf\_tunnel\_name) | A user-friendly name chosen when the tunnel is created | `string` | `"example-tunnel"` | no |
| <a name="input_cf_version"></a> [cf\_version](#input\_cf\_version) | Cloudflare Tunnel client version (formerly Argo Tunnel) | `string` | `"latest"` | no |
| <a name="input_cf_zone_name"></a> [cf\_zone\_name](#input\_cf\_zone\_name) | The name of the zone | `string` | n/a | yes |
| <a name="input_k8s_namespace"></a> [k8s\_namespace](#input\_k8s\_namespace) | Namespace defines the space within which name of the deployment must be unique | `string` | `"default"` | no |
| <a name="input_replicas"></a> [replicas](#input\_replicas) | The number of desired Cloudflared Pod replicas | `string` | `"1"` | no |
| <a name="input_tunnel_routes"></a> [tunnel\_routes](#input\_tunnel\_routes) | Service Routes to create, maps DNS hostname to internal K8s service | `map(string)` | <pre>{<br>  "hello": "hello_world"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cf_tunnel_cname"></a> [cf\_tunnel\_cname](#output\_cf\_tunnel\_cname) | Usable CNAME for accessing the Argo Tunnel |
| <a name="output_cf_tunnel_id"></a> [cf\_tunnel\_id](#output\_cf\_tunnel\_id) | The Argo Tunnel UUID |
| <a name="output_cf_zone_id"></a> [cf\_zone\_id](#output\_cf\_zone\_id) | Cloudflare zone ID |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Contributing
Code quality and security will be validated before merge requests are accepted.

### Tools
These tools are used to ensure validation and standardization of Terraform deployments

#### Must be installed
- [pre-commit](https://github.com/gruntwork-io/pre-commit/releases)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)

#### Provided by Terraform
- [terraform fmt](https://www.terraform.io/docs/commands/fmt.html)
- [terraform validate](https://www.terraform.io/docs/commands/validate.html)

For more information see - [pre-commit-hooks-for-terraform](https://medium.com/slalom-build/pre-commit-hooks-for-terraform-9356ee6db882)

### To submit a merge request
```bash
git checkout -b <branch name>
pre-commit autoupdate
pre-commit run -a
git commit -a -m 'Add new feature'
git push origin <branch name>
```
Optionally run the following to automate the execution of pre-commit on every git commit.
```bash
pre-commit install
```

# License
Copyright (c) 2023 [sms.com](www.sms.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
