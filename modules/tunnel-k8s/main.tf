terraform {
  required_version = "~> v1.4.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.4.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.14.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.18.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.0"
    }
  }
}

data "cloudflare_zone" "zone" {
  name = var.cf_zone_name
}

# The random_id resource is used to generate a 35 character secret for the tunnel
resource "random_id" "tunnel_secret" {
  byte_length = 35
}

resource "cloudflare_tunnel" "tunnel" {
  account_id = var.cf_account_id
  name       = var.cf_tunnel_name
  secret     = random_id.tunnel_secret.b64_std
}

resource "cloudflare_record" "service" {
  for_each = var.tunnel_routes

  zone_id = data.cloudflare_zone.zone.zone_id
  name    = each.key
  value   = cloudflare_tunnel.tunnel.cname
  type    = "CNAME"
  ttl     = 1
  proxied = true
}

resource "kubernetes_secret" "cf_tunnel_creds" {
  metadata {
    name      = "tunnel-credentials"
    namespace = var.k8s_namespace
  }
  data = {
    "credentials.json" = jsonencode({
      "AccountTag"   = var.cf_account_id,
      "TunnelSecret" = random_id.tunnel_secret.b64_std,
      "TunnelID"     = cloudflare_tunnel.tunnel.id
    })
  }
}

resource "kubectl_manifest" "cf_tunnel_configmap" {
  yaml_body = templatefile("${path.module}/manifests/cloudflared_configmap.yaml", {
    namespace  = var.k8s_namespace
    tunnelname = var.cf_tunnel_name
    routes     = var.tunnel_routes
    zonename   = var.cf_zone_name
  })

  ignore_fields = ["metadata.managedFields"]
}

resource "kubectl_manifest" "cf_tunnel_deployment" {
  yaml_body = templatefile("${path.module}/manifests/cloudflared_deployment.yaml", {
    namespace = var.k8s_namespace
    replicas  = var.replicas
    version   = var.cf_version
  })

  ignore_fields = ["metadata.annotations"]
}
