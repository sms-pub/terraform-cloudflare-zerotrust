terraform {
  required_version = "~> v1.4.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.4.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.9.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.0"
    }
  }
}

data "cloudflare_zone" "zone" {
  name = var.cf_zone_name
}

# The random_id resource is used to generate a 35 character secret for the tunnel
resource "random_id" "tunnel_secret" {
  byte_length = 35
}

resource "cloudflare_tunnel" "tunnel" {
  account_id = var.cf_account_id
  name       = var.cf_tunnel_name
  secret     = random_id.tunnel_secret.b64_std
}

resource "cloudflare_record" "service" {
  for_each = var.tunnel_routes

  zone_id = data.cloudflare_zone.zone.zone_id
  name    = each.key
  value   = cloudflare_tunnel.tunnel.cname
  type    = "CNAME"
  ttl     = 1
  proxied = true
}

resource "helm_release" "cloudflare_tunnel" {
  name      = "cloudflared"
  chart     = "${path.module}/charts/cloudflare-tunnel"
  namespace = var.k8s_namespace

  set {
    name  = "image.tag"
    value = var.cf_version
  }
  set {
    name  = "replicaCount"
    value = var.replicas
  }
  set {
    name  = "cloudflare.account"
    value = var.cf_account_id
  }
  set {
    name  = "cloudflare.tunnelName"
    value = var.cf_tunnel_name
  }
  set {
    name  = "cloudflare.tunnelId"
    value = cloudflare_tunnel.tunnel.id
  }
  set {
    name  = "cloudflare.secret"
    value = random_id.tunnel_secret.b64_std
  }
  dynamic "set" {
    for_each = keys(var.tunnel_routes)
    content {
      name  = "cloudflare.ingress[${set.key}].hostname"
      value = "${set.value}.${var.cf_zone_name}"
    }
  }
  dynamic "set" {
    for_each = values(var.tunnel_routes)
    content {
      name  = "cloudflare.ingress[${set.key}].service"
      value = set.value
    }
  }
}
