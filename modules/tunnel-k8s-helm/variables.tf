# Kubernetes variables
variable "k8s_namespace" {
  description = "Namespace defines the space within which name of the deployment must be unique"
  type        = string
  default     = "default"
}

# Cloudflare Variables
variable "cf_account_id" {
  description = "The Cloudflare account ID that you wish to manage the Argo Tunnel on"
  type        = string
}

variable "cf_tunnel_name" {
  description = "A user-friendly name chosen when the tunnel is created"
  type        = string
  default     = "example-tunnel"
}

variable "cf_version" {
  description = "Cloudflare Tunnel client version (formerly Argo Tunnel)"
  type        = string
  default     = "latest"
}

variable "replicas" {
  description = "The number of desired Cloudflared Pod replicas"
  type        = string
  default     = "1"
}

variable "cf_zone_name" {
  description = "The name of the zone"
  type        = string
}

variable "tunnel_routes" {
  description = "Service Routes to create, maps DNS hostname to internal K8s service"
  type        = map(string)
  default = {
    hello = "hello_world"
  }
}
