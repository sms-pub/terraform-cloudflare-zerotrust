output "cf_zone_id" {
  description = "Cloudflare Zone ID"
  value       = data.cloudflare_zone.zone.zone_id
}

output "cf_tunnel_id" {
  description = "The Cloudflare Tunnel UUID"
  value       = cloudflare_tunnel.tunnel.id
}

output "cf_tunnel_cname" {
  description = "Usable CNAME for accessing the Cloudflare Tunnel"
  value       = cloudflare_tunnel.tunnel.cname
}

output "cf_tunnel_script" {
  description = "Returns a user data script for provisioning instances to connect to the Cloudflare Tunnel"
  value       = local.user_data
  sensitive   = true
}
