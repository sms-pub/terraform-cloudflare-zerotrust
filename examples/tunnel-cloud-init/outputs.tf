output "tunnel_module_output" {
  description = "Return all module outputs"
  value       = module.example_tunnel[*]
  sensitive   = true
}

output "instance_module_output" {
  description = "Return all module outputs"
  value       = module.instance[*]
}
