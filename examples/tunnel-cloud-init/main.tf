terraform {
  required_version = "~> v1.4.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.4.0"
    }
    cloudinit = {
      source  = "hashicorp/cloudinit"
      version = "2.3.2"
    }
    vsphere = {
      source  = "hashicorp/vsphere"
      version = ">= 2.4.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cf_api_token
}

provider "vsphere" {
  vsphere_server       = var.vsphere_server
  user                 = var.vsphere_username
  password             = var.vsphere_password
  allow_unverified_ssl = var.vsphere_insecure
}

# Expose the demo app through Cloudflare ZeroTrust
module "example_tunnel" {
  source = "../../modules/tunnel-cloud-init"

  cf_account_id = var.cf_account_id
  cf_zone_name  = var.cf_zone_name

  warp_routing = var.warp_routing

  ingress_rules = var.ingress_rules
  tunnel_routes = var.tunnel_routes
}

data "cloudinit_config" "provision" {
  gzip          = false
  base64_encode = true

  part {
    filename     = "script.sh"
    content_type = "text/x-shellscript"

    content = module.example_tunnel.cf_tunnel_script
  }

  part {
    filename     = "userdata.yml"
    content_type = "text/cloud-config"

    content = templatefile("${path.module}/userdata.yml.tftpl", {
      user_name          = var.user_name
      user_passwd        = var.user_passwd
      ssh_authorized_key = var.ssh_authorized_key
      package_list       = var.package_list
    })
  }
}

module "instance" {
  source = "git::git@gitlab.sms.com:sms/artifacts/infrastructure-modules/terraform-vsphere-instance//modules/vsphere-virtual-machine/template-linux-cloud-init?ref=v1.1.0"

  # vSphere Settings
  vsphere_datacenter = var.vsphere_datacenter
  vsphere_cluster    = var.vsphere_cluster
  vsphere_folder     = var.vsphere_folder
  vsphere_datastore  = var.vsphere_datastore
  vsphere_network    = var.vsphere_network
  vsphere_template   = var.vsphere_template

  # Virtual Machines Settings
  vm_name                    = var.vm_name
  vm_cpus                    = var.vm_cpus
  vm_memory                  = var.vm_memory
  vm_disk_size               = var.vm_disk_size
  vm_efi_secure_boot_enabled = var.vm_efi_secure_boot_enabled

  guestinfo_metadata = base64encode(templatefile("${path.module}/metadata.yml.tftpl", {
    vm_name = var.vm_name
  }))
  guestinfo_userdata = data.cloudinit_config.provision.rendered

  depends_on = [module.example_tunnel]
}
