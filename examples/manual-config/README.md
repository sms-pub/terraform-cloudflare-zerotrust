# named-tunnel-k8s
## Reference
Cloudflare Docs - https://developers.cloudflare.com/cloudflare-one/tutorials/many-cfd-one-tunnel/
Github Examples - https://github.com/cloudflare/argo-tunnel-examples/tree/master/named-tunnel-k8s

## Install Cloudflared
```bash
wget https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb
sudo dpkg -i cloudflared-linux-amd64.deb
```

## Authenticate to Cloudflare and generate credentials
```bash
$ cloudflared tunnel login

A browser window should have opened at the following URL:

https://dash.cloudflare.com/argotunnel?callback=https%3A%2F%2Flogin.cloudflareaccess.org%2FJE0-sBZnQUFn188u1T6KVa2SWasqNTYnlaVp4jqfuzU%3D

If the browser failed to open, please visit the URL above directly in your browser.
You have successfully logged in.
If you wish to copy your credentials to a server, they have been saved to:
/home/user/.cloudflared/cert.pem
```

## Create Argo Tunnel
```bash
$ cloudflared tunnel create example-tunnel

Tunnel credentials written to /home/user/.cloudflared/7aaa556c-9e0f-4148-8406-37204d776cef.json. cloudflared chose this file based on where your origin certificate was found. Keep this file secret. To revoke these credentials, delete the tunnel.

Created tunnel example-tunnel with id 7aaa556c-9e0f-4148-8406-37204d776cef
```

## Upload Credentials to Cluster
```bash
kubectl create secret generic tunnel-credentials -n cattle-system \
  --from-file=credentials.json=/home/user/.cloudflared/7aaa556c-9e0f-4148-8406-37204d776cef.json
```

## Create CNAME
Create a Cloudflare CNAME record pointing the application host name to the tunnel CNAME.

`tunnel.example.com -> 7aaa556c-9e0f-4148-8406-37204d776cef.cfargotunnel.com`

`hello.example.com -> 7aaa556c-9e0f-4148-8406-37204d776cef.cfargotunnel.com`

## Create Cloudflared Deployment
Update the example manifest and deploy;
```bash
kubectl apply -f app.yaml
kubectl apply -f cloudflared.yaml
```
