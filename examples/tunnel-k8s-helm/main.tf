terraform {
  required_version = "~> v1.4.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.4.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.9.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.14.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cf_api_token
}

provider "helm" {
  kubernetes {
    config_path    = var.config_path
    config_context = var.config_context
  }
}

provider "kubectl" {
  config_path    = var.config_path
  config_context = var.config_context
}

# Split multi-document yaml file. See - https://registry.terraform.io/providers/gavinbunney/kubectl/latest/docs/data-sources/kubectl_file_documents
data "kubectl_file_documents" "docs" {
  content = file("../manual-config/app.yaml")
}

# Deploy a demo app to a K8s cluster - https://hub.docker.com/r/kennethreitz/httpbin/
resource "kubectl_manifest" "test" {
  for_each  = data.kubectl_file_documents.docs.manifests
  yaml_body = each.value
}

# Expose the demo app through Cloudflare ZeroTrust
module "example_tunnel" {
  source = "../../modules/tunnel-k8s-helm"

  cf_account_id = var.cf_account_id
  cf_zone_name  = var.cf_zone_name

  tunnel_routes = var.tunnel_routes
}
