output "module_output" {
  description = "Return all module outputs"
  value       = module.example_tunnel[*]
}
