terraform {
  required_version = "~> v1.4.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.4.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cf_api_token
}

# Expose the demo app through Cloudflare ZeroTrust
module "example_tunnel" {
  source = "../../modules/tunnel-remote-exec"

  cf_account_id = var.cf_account_id
  cf_zone_name  = var.cf_zone_name

  warp_routing = var.warp_routing

  ingress_rules = var.ingress_rules
  tunnel_routes = var.tunnel_routes

  connector_hosts = var.connector_hosts
  #  ssh_user        = var.ssh_user
}
