# Cloudflare Variables
variable "cf_api_token" {
  description = "Cloudflare API Token with Zone permissions. See - https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/deployment-guides/terraform/#3-create-a-cloudflare-api-token"
  type        = string
  sensitive   = true
}

variable "cf_account_id" {
  description = "The Cloudflare account ID that you wish to manage the Cloudflare Tunnel on"
  type        = string
}

variable "cf_zone_name" {
  description = "The name of the zone"
  type        = string
}

variable "warp_routing" {
  description = "If you're exposing a private network, you need to add the warp-routing key and set it to true."
  type        = bool
  default     = true
}

variable "ingress_rules" {
  description = "Service Routes to create, maps DNS CNAME to internal services"
  type = list(object({
    hostname = string
    path     = string
    service  = string
  }))
  default = [
    {
      hostname = "myweb"
      path     = ""
      service  = "http://web-service:80"
    },
    {
      hostname = "hello"
      path     = ""
      service  = "hello_world"
    }
  ]
}

variable "tunnel_routes" {
  description = "The IPv4 or IPv6 network for the tunnel route, in CIDR notation."
  type        = map(string)
  default = {
    "10.0.0.0/8" = "Private Network"
  }
}

# Connector Variables
variable "connector_hosts" {
  description = "Hosts to configure for connection to the Cloudflare tunnel"
  type        = list(string)
  default     = []
}

#variable "ssh_user" {
#  description = "Host ssh user name"
#  type        = string
#  sensitive   = true
#}
