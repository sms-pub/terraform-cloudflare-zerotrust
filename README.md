# terraform-cloudflare-zerotrust

This repo contains modules for deploying Cloudflare resources.

<!-- UPDATE THIS SECTION WHEN ADDING NEW MODULES -->
* [tunnel-cloud-init](/modules/tunnel-k8s): This module deploys a Cloudflare Tunnel and configuration and returns a provisioning script for use with [cloud-init](https://cloudinit.readthedocs.io/).
* [tunnel-k8s](/modules/tunnel-k8s): Used to expose a kubernets application with [Cloudflared](https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/) and [Cloudflare Zero Trust](https://developers.cloudflare.com/cloudflare-one/).
* [tunnel-k8s-helm](/modules/tunnel-k8s-helm): Expose a kubernetes cluster with Cloudflare Tunnel Helm Chart. See - [argo-tunnel-examples](https://github.com/cloudflare/argo-tunnel-examples/tree/master/helm/cloudflare-tunnel)
* [tunnel-remote-exec](/modules/tunnel-k8s): Provisions an existing linux host with [Cloudflared](https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/) to expose applications with [Cloudflare Zero Trust](https://developers.cloudflare.com/cloudflare-one/).

Click on each module above to see its documentation. Head over to the [examples folder](/examples) for examples.

## TODO
- Expose more configuration options

## Contributing
Code quality and security will be validated before merge requests are accepted.

### Tools
These tools are used to ensure validation and standardization of Terraform deployments

#### Must be installed
- [pre-commit](https://github.com/gruntwork-io/pre-commit/releases)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)

#### Provided by Terraform
- [terraform fmt](https://www.terraform.io/docs/commands/fmt.html)
- [terraform validate](https://www.terraform.io/docs/commands/validate.html)

For more information see - [pre-commit-hooks-for-terraform](https://medium.com/slalom-build/pre-commit-hooks-for-terraform-9356ee6db882)

### To submit a merge request
```bash
git checkout -b <branch name>
pre-commit autoupdate
pre-commit run -a
git commit -a -m 'Add new feature'
git push origin <branch name>
```
Optionally run the following to automate the execution of pre-commit on every git commit.
```bash
pre-commit install
```

# License
Copyright (c) 2023 [sms.com](www.sms.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
